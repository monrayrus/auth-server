package ru.khusnullin.authserver.repository;

import org.springframework.data.repository.CrudRepository;
import ru.khusnullin.authserver.models.User;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
